let http = require("http");

let courses = [];


http.createServer(function(req,res){
	
	if (req.url == "/"){
		res.writeHead(200, {"Content-Type":"text/plain"});
		res.end("Welcome to Booking System");

	} else if (req.url == "/profile"){
		res.writeHead(200, {"Content-Type":"text/plain"});
		res.end("Welcome to your profile!");

	}else if (req.url == "/courses"  && req.method=="GET"){
		res.writeHead(200, {"Content-Type":"application/json"});
		res.write("Here's our courses available " + JSON.stringify(courses));
		res.end();
	}else if (req.url == "/updatecourse" && req.method=="PUT"){
		res.writeHead(200, {"Content-Type":"text/plain"});
		res.end("Update a course to our resources");
		

	}else if (req.url == "/archivecourse" && req.method=="DELETE"){
		res.writeHead(200, {"Content-Type":"text/plain"});
		res.end("Archive courses to our resources");
		

	}else {
		res.writeHead(404, {"Content-Type":"text/plain"});
		res.end("404: Page not found.");
	}

}).listen(4000);

console.log(`Server now accessible at localhost 4000`);